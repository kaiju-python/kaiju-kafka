
.. image:: https://badge.fury.io/py/kaiju-kafka.svg
    :target: https://pypi.org/project/kaiju-kafka
    :alt: Latest package version

.. image:: https://readthedocs.org/projects/kaiju-kafka/badge/?version=latest
    :target: https://kaiju-kafka.readthedocs.io/en/latest/?badge=latest
    :alt: Documentation Status

Summary
-------

**Python** >=3.8

**Postgresql** >=14

`Project documentation <https://kaiju-kafka.readthedocs.io/en/latest/>`_

Database transport, interfaces and migration services for postgresql.

Use `pip install kaiju-kafka` to install the package.

For development run the init script `tools/init.sh`.
It will setup git hooks and install the dev version of the project.

Testing
_______

- Install the package with dev dependencies using `tools/init.sh` or `pip install -e .[test]`.
- Start docker daemon.
- Run `pytest` from the project root.

Documentation
_____________

Run `docs/docs.sh` from the project root to generate documentation.
