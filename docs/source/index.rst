
.. raw:: html

   <hr>

.. include:: ../../README.rst
.. include:: license.rst

Reference
---------

.. toctree::
  :maxdepth: 2

  modules
